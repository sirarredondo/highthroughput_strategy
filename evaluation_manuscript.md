Code to reproduce the main findings reported in the manuscript
================

# R packages

``` r
library(igraph)
library(mlplasmids)
library(tidyverse)
library(cowplot)
library(ggrepel)
```

# Barcode - Illumina id

``` r
barcode_illumina <- read.csv(file = 'Metadata/sample_sheet_Samuelsen96_1.txt', sep = '\t')
barcode_illumina <- select(barcode_illumina, c('Barcode','Lane'))
colnames(barcode_illumina)[2] <- 'Isolate'
```

# Nanostats summary

Bash code to summarize the results given by Nanostats

``` bash
for i in run96barcode*; do awk -v var="$i" '{print $N,"\t",var}' $i/*.txt; done > nanostats_summary.txt
grep 'Total bases' nanostats_summary.txt > total_bases.txt
grep 'Read length N50' nanostats_summary.txt > n50_read_length.txt
grep 'Mean read quality:' nanostats_summary.txt > phred_read_quality.txt

sed -i 's/    /\t/g' total_bases.txt 

sed -i 's/    /\t/g' n50_read_length.txt 
sed -i 's/    /\t/g' n50_read_length.txt 
sed -i 's/\t/,/g' n50_read_length.txt 
sed -i 's/,,,/,/g' n50_read_length.txt

sed -i 's/    /\t/g' phred_read_quality.txt 
sed -i 's/\t/,/g' phred_read_quality.txt
sed -i 's/,,,,/,/g' phred_read_quality.txt
sed -i 's/,/\t/g' phred_read_quality.txt 
```

# Results section: Uneven distribution of ONT reads in the 96 multiplexing approach

``` r
# Results section 'Uneven distribution of ONT reads in the 96 multiplexing approach'

total_ont_bases <- read.csv(file = 'Nanostat/total_bases.txt', header = FALSE, sep = '\t')
total_ont_bases$Bases <- as.numeric(gsub(pattern = ',', replacement = '', x = total_ont_bases$V4))

round(sum(total_ont_bases$Bases)/1e9,2) # First result section 'Uneven distribution of ONT reads in the 96 multiplexing approach' 
```

    ## [1] 10.71

``` r
n50_ont_length <- read.csv(file = 'Nanostat/n50_read_length.txt', header = FALSE)
n50_ont_length$Length <- as.numeric(paste(n50_ont_length$V2, n50_ont_length$V3, sep = ''))
round(summary(n50_ont_length$Length)/1e3,2) # Second result section 'Uneven distribution of ONT reads in the 96 multiplexing approach'
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##    0.21   18.43   21.96   20.98   26.05   32.48

``` r
phred_ont_quality <- read.csv(file = 'Nanostat/phred_read_quality.txt', header = FALSE, sep = '\t')
phred_ont_quality$Phred_score <- as.numeric(phred_ont_quality$V2)
round(summary(phred_ont_quality$Phred_score),2) # Third result section 'Uneven distribution of ONT reads in the 96 multiplexing approach'
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##   11.70   12.30   12.50   12.48   12.60   12.90

``` r
error <- 10^-(round(summary(phred_ont_quality$Phred_score)[4],2)/10)
acc_perc <- (1-error)*100
round(acc_perc, 2) # Fourth result section 'Uneven distribution of ONT reads in the 96 multiplexing approach' 
```

    ##  Mean 
    ## 94.35

``` r
n50_barcode <- ggplot(n50_ont_length, aes(x = '', y = Length/1e3)) + geom_boxplot() + geom_point() + labs(y = 'N50 ONT read length (kbp)', x = '')

mbp_barcode <- ggplot(total_ont_bases, aes(x = '', y = Bases/1e6)) + geom_boxplot() + geom_point() + labs(y = 'Total ONT bases (Mbp)', x = '')

phred_barcode <- ggplot(phred_ont_quality, aes(x = '', y = Phred_score)) + geom_boxplot() + geom_point() + labs(y = 'Phred score (log scale)', x = '')

plot_grid(mbp_barcode, n50_barcode, phred_barcode, ncol = 3)
```

![](evaluation_manuscript_files/figure-gfm/ontreads-1.png)<!-- -->

``` r
#### Supplementary Table S1 

# Creating a Suppl. Table with all this information 

n50_sel <- subset(n50_ont_length, select = c('V4','Length'))
colnames(n50_sel) <- c('Barcode','Avg_Read_Length')

total_ont_sel <- subset(total_ont_bases, select = c('V5','Bases'))
colnames(total_ont_sel) <- c('Barcode','ONT_bases')

phred_sel <- subset(phred_ont_quality, select = c('V3','Phred_score'))
colnames(phred_sel) <- c('Barcode','Avg_Phred_Score')

suppl_table_S1 <- merge(n50_sel, total_ont_sel, by = 'Barcode')

suppl_table_S1$Barcode <- gsub(pattern = ' ', replacement = '', x = suppl_table_S1$Barcode)

suppl_table_S1 <- merge(suppl_table_S1, phred_sel, by = 'Barcode')


suppl_table_S1$Barcode <- gsub(pattern = 'run96barcode', replacement = '', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '01', replacement = '1', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '02', replacement = '2', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '03', replacement = '3', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '04', replacement = '4', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '05', replacement = '5', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '06', replacement = '6', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '07', replacement = '7', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '08', replacement = '8', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- gsub(pattern = '09', replacement = '9', x = suppl_table_S1$Barcode)
suppl_table_S1$Barcode <- as.numeric(suppl_table_S1$Barcode)


suppl_table_S1 <- merge(suppl_table_S1, barcode_illumina, by = 'Barcode')

# Loading the information with the ERR Illumina runs 

err_norm <- read.csv(file = 'Metadata/norm_metadata.csv', header = TRUE)
err_norm <- subset(err_norm, select = c('lane','Run.accession'))

colnames(err_norm) <- c('Isolate','Illumina_reads_accession')

suppl_table_S1 <- merge(suppl_table_S1, err_norm, by = 'Isolate')

suppl_table_S1$Approx_coverage <- suppl_table_S1$ONT_bases/5e6


suppl_table_S1 <- subset(suppl_table_S1, select = c('Isolate','Barcode','ONT_bases','Approx_coverage','Avg_Read_Length','Avg_Phred_Score','Illumina_reads_accession'))

ont_accessions <- read.csv(file = 'Metadata/ont_accessions.csv', header = FALSE)

ont_accessions <- subset(ont_accessions, ont_accessions$V4 == ont_accessions$V13) # Sanity check 

ont_accessions$Isolate <- gsub(pattern = 'webin-reads-ONT_', replacement = '', x =ont_accessions$V1)
ont_accessions$ONT_reads_accession <- ont_accessions$V10

ont_accessions_info <- subset(ont_accessions, select = c('Isolate','ONT_reads_accession'))

suppl_table_S1 <- merge(ont_accessions_info, suppl_table_S1)

suppl_table_S1 <- suppl_table_S1[order(suppl_table_S1$Barcode, decreasing = FALSE),]

suppl_table_S1$Barcode <- paste('Barcode', suppl_table_S1$Barcode, sep = '')

write.csv(x = suppl_table_S1, file = 'Suppl_Table_S1.csv', quote = FALSE, row.names = FALSE)

#### Supplementary Table S1 


# Merging the total ONT bases with the illumina id that we will use to identify the isolates in the subsequent analyses

total_ont_bases$Barcode <- gsub(pattern = 'run96barcode', replacement = '', x = total_ont_bases$V5)
total_ont_bases$Barcode <- gsub(pattern = '01', replacement = '1', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- gsub(pattern = '02', replacement = '2', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- gsub(pattern = '03', replacement = '3', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- gsub(pattern = '04', replacement = '4', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- gsub(pattern = '05', replacement = '5', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- gsub(pattern = '06', replacement = '6', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- gsub(pattern = '07', replacement = '7', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- gsub(pattern = '08', replacement = '8', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- gsub(pattern = '09', replacement = '9', x = total_ont_bases$Barcode)
total_ont_bases$Barcode <- as.numeric(total_ont_bases$Barcode)

round(summary(total_ont_bases$Bases/1e6),2) # Fifht and sixth results in section 'Uneven distribution of ONT reads in the 96 multiplexing approach'
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##    3.37   56.94  103.45  111.53  172.45  244.00

``` r
round(min(total_ont_bases$Bases/1e6),2) # Seventh result in section 'Uneven distribution of ONT reads in the 96 multiplexing approach'
```

    ## [1] 3.37

``` r
round(max(total_ont_bases$Bases/1e6),2) # Eigth result in section 'Uneven distribution of ONT reads in the 96 multiplexing approach'
```

    ## [1] 244

``` r
barcode_coverage <- subset(total_ont_bases, select = c('Barcode','Bases'))

barcode_information <- merge(barcode_coverage, barcode_illumina, by = 'Barcode')
```

# Results section: Evaluation of the hybrid assemblies

## Nanopore reads aligning to the short-read assembly given by Unicycler

We extracted from the log given by Unicycler, the number of long-reads
mapping to the short-read assembly and thus being used by Unicycler to
create the hybrid assembly

``` bash
grep -B 3 -A 1 'Total bases aligned' *unicycler.log > read_report-current_strategy.txt 
```

``` r
read_current_strategy <- read_table(file = 'Metadata/read_report-current_strategy.txt', col_names = FALSE)
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   X1 = col_character()
    ## )

``` r
read_current_strategy$Strategy <- 'Barcode-96'

read_current_strategy <- subset(read_current_strategy, read_current_strategy$X1 != '--')

read_current_strategy$value <- str_split_fixed(string = read_current_strategy$X1, pattern = ' ', n = 5)[,5]
read_current_strategy$value <- gsub(pattern = ' ', replacement = '', x = read_current_strategy$value)
read_current_strategy$value <- gsub(pattern = ',', replacement = '', x = read_current_strategy$value)
read_current_strategy$value <- gsub(pattern = '%', replacement = '', x = read_current_strategy$value)
read_current_strategy$value <- gsub(pattern = 'bp', replacement = '', x = read_current_strategy$value)
read_current_strategy$value <- as.numeric(read_current_strategy$value)

read_current_strategy$Metric <- str_split_fixed(string = read_current_strategy$X1, pattern = 'unicycler.log', n = 2)[,2]
read_current_strategy$Metric <- str_split_fixed(string = read_current_strategy$Metric, pattern = ' ', n = 2)[,1]

read_current_strategy$Metric <- substring(read_current_strategy$Metric, 2)
read_current_strategy$Metric <- str_split_fixed(string = read_current_strategy$Metric, pattern = ':', n = 2)[,1]

total_reads <- subset(read_current_strategy, read_current_strategy$Metric == 'Total')

round(summary(total_reads$value/1e6),2) # 9th and 10th results in section 'Uneven distribution of ONT reads in the 96 multiplexing approach'
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##    2.98   47.70   85.39   94.36  142.02  202.55

``` r
total_reads$value <- total_reads$value/5e6
total_reads$Metric <- 'Coverage'

# Joinnng the hybrid assemblies together with the short-read assembly information and read coverage

total_reads$X1 <- gsub(pattern = 'phred20_unicycler/unicycler.log', replacement = '', x = total_reads$X1)

total_reads$Illumina_id <- str_split_fixed(string = total_reads$X1, pattern = ':', n = 2)[,1]
total_reads$Illumina_id <- str_split_fixed(string = total_reads$Illumina_id, pattern = '_unicycler', n = 2)[,1]
total_reads$X1 <- NULL
```

We read the contigs present in the assembly graph by Unicycler and
classify them as plasmid or chromosome using mlplasmids. Then, we
compute several statistics to assess the assemblies

``` r
list_assemblies <- read_table(file = 'Unicycler/list_unicycler.txt', col_names = FALSE)

uni_evaluation <- NULL
comp_uni <- NULL
for(assembly in list_assemblies$X1)
{
  file_path <- paste('Unicycler/', assembly, sep = '')
  uni_links <- read.table(file = file_path, comment.char = "")
  filtered_graph <- data.frame(From_to = uni_links$V2, To_from = uni_links$V4) 
  
    # We use the function from igraph to create a network from a dataframe
  graph_pairs <<- graph_from_data_frame(filtered_graph, directed = FALSE)

  contigs_clusterg <- data.frame(edge = names(components(graph_pairs)$membership),
                                 Component = as.numeric(as.character(components(graph_pairs)$membership)))
  
  circularity_vector <- NULL
  
  for(component in contigs_clusterg$Component)
  {
    subgraph <- decompose(graph_pairs)[[component]]
    circularity <- is_connected(subgraph)
    circularity_vector <- append(x = circularity_vector, values = circularity, after = length(circularity_vector))
  }

  contigs_clusterg$Circularity <- circularity_vector
    
  contigs_per_component <- as.numeric(as.character(components(graph_pairs)$csize))
  
  contigs_clusterg$Number_contigs <- contigs_per_component[contigs_clusterg$Component]
  
  contigs_clusterg$Contig_name <- gsub(pattern = 'edge_', replacement = 'contig_', x = contigs_clusterg$edge)
  
  contigs_clusterg$edge <- NULL
  
  assembly_name <- gsub(pattern = 'phred20_unicycler_links.txt', replacement = '', x = assembly)
  
  path_assembly <- paste('Unicycler/',assembly_name, sep = '')
  
  path_assembly <- paste(path_assembly, 'phred20_unicycler_segments.fa', sep = '')
  
  
  example_prediction <- suppressMessages(plasmid_classification(path_input_file = path_assembly, species = 'Escherichia coli', full_output = TRUE, min_length = 1))
  
  example_prediction$Contig_name <- str_split_fixed(string = example_prediction$Contig_name, pattern = '_', n = 2)[,1]
  example_prediction$Contig_name <- gsub(pattern = 'S', replacement = '', x = example_prediction$Contig_name)
  
  component_contigs <- merge(example_prediction, contigs_clusterg, by = 'Contig_name')
  
  independent_contigs <- subset(example_prediction,! example_prediction$Contig_name %in% contigs_clusterg$Contig_name)
  if(nrow(independent_contigs) > 0)
  {
      independent_contigs$Component <- paste('independent',independent_contigs$Contig_name, sep = '_')
      independent_contigs$Number_contigs <- 1
      independent_contigs$Circularity <- 'FALSE'
      independent_contigs <- subset(independent_contigs, select = colnames(component_contigs))
      component_contigs <- rbind(component_contigs, independent_contigs)
  
  }
  
  n50_evaluation <- NULL
  
  
  for(component in unique(component_contigs$Component))
  {
    ind_component <- subset(component_contigs, component_contigs$Component == component)
    ind_component <- ind_component[order(ind_component$Contig_length, decreasing = TRUE),]
    
    component_size <- sum(ind_component$Contig_length)
    half_component_size <- component_size*0.5
    
    n50_sum <- 0
    
    prediction_contig <- ind_component$Prediction[1]
    prob_contig <- ind_component$Prob_Chromosome[1]
    circularity_contig <- ind_component$Circularity[1]
    
    for(contig in ind_component$Contig_name)
    {
      ind_contig <- subset(ind_component, ind_component$Contig_name == contig)
      n50_sum <- n50_sum + ind_contig$Contig_length
      if(n50_sum > half_component_size)
      {
        n50_df <- data.frame(Assembly = assembly_name, 
                   Component = component,
                   Prediction = prediction_contig,
                   Prob_chr = prob_contig,
                   Number_contigs = ind_component$Number_contigs,
                   Component_size = component_size,
                   N50 = ind_contig$Contig_length,
                   Circularity = circularity_contig)
        
        n50_evaluation <- rbind(n50_evaluation, n50_df)
    
        break
        
      }
      
    }
  }
  component_contigs$Isolate <- assembly_name
  comp_uni <- rbind(comp_uni, component_contigs)
  uni_evaluation <- rbind(uni_evaluation, n50_evaluation)

  
}

uni_evaluation$Isolate_Component <- paste(uni_evaluation$Assembly, uni_evaluation$Component, sep = '_')
uni_evaluation <- uni_evaluation[! duplicated(uni_evaluation$Isolate_Component),]

uni_evaluation$Contiguity <- uni_evaluation$N50/uni_evaluation$Component_size
uni_evaluation$Illumina_id <- uni_evaluation$Assembly

uni_evaluation <- merge(uni_evaluation, total_reads, by = 'Illumina_id')


# The number of the components is not sorted based on their component size, let's tackle this 

uni_modification <- uni_evaluation
uni_modification$Component <- NULL
uni_modification$Isolate_Component <- NULL

uni_evaluation <- NULL

for(isol in unique(uni_modification$Illumina_id))
{
  particular_isolate <- subset(uni_modification, uni_modification$Illumina_id == isol)
  particular_isolate <- particular_isolate[order(particular_isolate$Component_size, decreasing = TRUE),]
  particular_isolate$Component<- c(1:nrow(particular_isolate))
  uni_evaluation <- rbind(particular_isolate, uni_evaluation)
  
}


####### Supplementary Table S2 

suppl_table_S2 <- uni_evaluation

suppl_table_S2 <- subset(suppl_table_S2, select = c('Illumina_id','Component','Component_size','N50','Contiguity','Number_contigs','Circularity','Prediction','Prob_chr'))

colnames(suppl_table_S2)[1] <- 'Isolate'
colnames(suppl_table_S2)[8] <- 'mlplasmids_prediction'
colnames(suppl_table_S2)[9] <- 'mlplasmids_chromosome_posterior_probability'

suppl_table_S2 <- merge(suppl_table_S2, barcode_illumina, by = 'Isolate')

suppl_table_S2 <- suppl_table_S2[order(suppl_table_S2$Barcode, decreasing = FALSE),]

suppl_table_S2$Barcode <- paste('Barcode', suppl_table_S2$Barcode, sep = '')

suppl_table_S2$Assembler <- 'Unicycler'

write.csv(x = suppl_table_S2, file = 'Suppl_Table_S2.csv', quote = FALSE, row.names = FALSE)

####### Supplementary Table S2 


# Total number of components 

nrow(uni_evaluation) 
```

    ## [1] 335

``` r
# Chromosomal components 

chromosome_unicycler <- subset(uni_evaluation, uni_evaluation$Prob_chr > 0.8)

## Longest segment in the chromosome part 

chromosome_unicycler <- chromosome_unicycler[order(chromosome_unicycler$Component_size, decreasing = TRUE),]

chromosomal_replicon_unicycler <- chromosome_unicycler[!duplicated(chromosome_unicycler$Assembly),]

nrow(chromosomal_replicon_unicycler) # 96 isolates, all in order 
```

    ## [1] 96

``` r
## Average size of the chromosomal component

round(mean(chromosomal_replicon_unicycler$Component_size)/1e6,2) # 1st result section 'Evaluation of hybrid assemblies'
```

    ## [1] 5

``` r
round(median(chromosomal_replicon_unicycler$Component_size)/1e6,2) # 2nd result section 'Evaluation of hybrid assemblies'
```

    ## [1] 5.03

``` r
## Average number of contigs in the chromosomal component 

round(mean(chromosomal_replicon_unicycler$Number_contigs),2) # 3rd result section 'Evaluation of hybrid assemblies'
```

    ## [1] 11.39

``` r
round(median(chromosomal_replicon_unicycler$Number_contigs),2) # 4th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 2

``` r
## Average contiguity values of the chromosomal component 

round(mean(chromosomal_replicon_unicycler$Contiguity),2) # 5th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 0.89

``` r
round(median(chromosomal_replicon_unicycler$Contiguity),2) # 6th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 1

``` r
## Number of circular chromosomal components 

circular_chromosomal_unicycler <- subset(chromosomal_replicon_unicycler, chromosomal_replicon_unicycler$Circularity == 'TRUE' & chromosomal_replicon_unicycler$Number_contigs == 1)
nrow(circular_chromosomal_unicycler)/96*100 # 7th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 50

``` r
## Correlation between the contiguity and the number of bases generated in the chromosomal component

round(cor(x = chromosomal_replicon_unicycler$Contiguity, y = chromosomal_replicon_unicycler$value, method = 'pearson'),2) # 8th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 0.41

``` r
ggplot(chromosomal_replicon_unicycler, aes(x = value, y = Contiguity)) + geom_point()
```

![](evaluation_manuscript_files/figure-gfm/unicycler_evaluation-1.png)<!-- -->

``` r
# Isolates with inferior contiguity values 

low_contiguity_chr_unicycler <- subset(chromosomal_replicon_unicycler, chromosomal_replicon_unicycler$Contiguity < 0.9) # 9th result section 'Evaluation of hybrid assemblies'

round(summary(low_contiguity_chr_unicycler$value*5e6/1e6),2) # 10th result section 'Evaluation of hybrid assemblies'
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##    2.98   17.70   36.56   42.64   67.69  102.45

``` r
round(summary(low_contiguity_chr_unicycler$value),2) # 11th result section 'Evaluation of hybrid assemblies'
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##    0.60    3.54    7.31    8.53   13.54   20.49

``` r
# Plasmid or extrachromosomal components 

plasmids_unicycler <- subset(uni_evaluation, uni_evaluation$Prob_chr <= 0.5)

## Large plasmids 

large_medium_plasmids_unicycler <- subset(plasmids_unicycler, plasmids_unicycler$Component_size >= 1e4)

nrow(large_medium_plasmids_unicycler) # 10th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 132

``` r
### Average contiguity values 

round(mean(large_medium_plasmids_unicycler$Contiguity),2) # 12th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 0.98

``` r
round(median(large_medium_plasmids_unicycler$Contiguity),2) # 13th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 1

``` r
### Average size

round(mean(large_medium_plasmids_unicycler$Number_contigs),2) # 16th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 1.79

``` r
round(median(large_medium_plasmids_unicycler$Number_contigs),2) # 17th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 1

``` r
### Circular medium and large plasmids

circular_medium_large_pl_uni <- subset(large_medium_plasmids_unicycler, large_medium_plasmids_unicycler$Circularity == 'TRUE' & large_medium_plasmids_unicycler$Number_contigs == 1)
nrow(circular_medium_large_pl_uni) # 14th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 118

``` r
nrow(circular_medium_large_pl_uni)/nrow(large_medium_plasmids_unicycler)*100 # 15th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 89.39394

``` r
### Correlation between the contiguity and the number of bases generated in the medium and large plasmids 

round(cor(x = large_medium_plasmids_unicycler$Contiguity, y = large_medium_plasmids_unicycler$value, method = 'pearson'),2) # 18th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 0.16

``` r
## Small plasmids 

small_plasmids_unicycler <- subset(plasmids_unicycler, plasmids_unicycler$Component_size < 10e3)

nrow(small_plasmids_unicycler) # 18th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 78

``` r
### Average contiguity values 

round(mean(small_plasmids_unicycler$Contiguity),2) # 19th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 0.99

``` r
round(median(small_plasmids_unicycler$Contiguity),2) # 20th result section 'Evaluation of hybrid assemblies'
```

    ## [1] 1

``` r
### Circular small plasmids

circular_small_pl_uni <- subset(small_plasmids_unicycler, small_plasmids_unicycler$Circularity == 'TRUE' & small_plasmids_unicycler$Number_contigs == 1)
round(nrow(circular_small_pl_uni),3) # 21st result section 'Evaluation of hybrid assemblies'
```

    ## [1] 74

``` r
round(nrow(circular_small_pl_uni)/nrow(small_plasmids_unicycler),3)*100 # 22nd result section 'Evaluation of hybrid assemblies'
```

    ## [1] 94.9

``` r
# More viz 

# Chromosome summary 

genomesize_chr_box <- ggplot(data = chromosomal_replicon_unicycler, aes(x = '', y = Component_size/1e6)) + geom_boxplot() + geom_point(size = 2.0) + labs(x = 'Unicycler assemblies', y = 'Chromosome Size (Mbp)')

contiguity_chr_box <- ggplot(data = chromosomal_replicon_unicycler, aes(x = '', y = Contiguity)) + geom_boxplot() + geom_point(size = 2.0) + labs(x = 'Unicycler assemblies', y = 'Chromosome Contiguity')

contiguity_chr_cov <- ggplot(data = chromosomal_replicon_unicycler, aes(x = value, y = Contiguity)) + geom_point(size = 2.0) + geom_smooth(se = FALSE) + labs(x = 'ONT bases (Mbp) - After Filtlong', y = 'Chromosome Contiguity')

# Plasmid summary 

genomesize_pl_box <- ggplot(data = large_medium_plasmids_unicycler, aes(x = '', y = Component_size/1e3)) + geom_boxplot() + geom_point(size = 2.0) + labs(x = 'Unicycler assemblies', y = 'Plasmid Size (kbp)')

contiguity_pl_box <- ggplot(data = large_medium_plasmids_unicycler, aes(x = '', y = Contiguity)) + geom_boxplot() + geom_point(size = 2.0) + labs(x = 'Unicycler assemblies', y = 'Plasmid Contiguity')

contiguity_pl_cov <- ggplot(data = large_medium_plasmids_unicycler, aes(x = value, y = Contiguity)) + geom_point(size = 2.0) + geom_smooth(se = FALSE) + labs(x = 'ONT bases (Mbp) - After Filtlong', y = 'Plasmid Contiguity')

plot_grid(genomesize_chr_box, contiguity_chr_box, contiguity_chr_cov, genomesize_pl_box, contiguity_pl_box, contiguity_pl_cov, ncol = 3, nrow = 2, labels = c('A','','','B','','')) # Figure 2 in section 'Evaluation of hybrid assemblies' 
```

![](evaluation_manuscript_files/figure-gfm/unicycler_evaluation-2.png)<!-- -->

# Results section: Underrepresentation of small and medium plasmids in the ONT library

``` r
# Longest segment in the assembly 

longest_segment_component <- comp_uni
longest_segment_component <- longest_segment_component[order(longest_segment_component$Contig_length, decreasing = TRUE),]
longest_segment_component$Isol_Component <- paste(longest_segment_component$Isolate, longest_segment_component$Component, sep = '_')
longest_segment_component <- longest_segment_component[! duplicated(longest_segment_component$Isol_Component),]

longest_segment_component$Isol_Contig <- paste(longest_segment_component$Isolate, longest_segment_component$Contig_name, sep = '_')

chromosome_longest_segment <- longest_segment_component[! duplicated(longest_segment_component$Isolate),]

list_files <- read.csv(file = 'Depth/list_illumina_file.txt', sep = '\t', header = FALSE)

all_illumina <- NULL
for(file in list_files$V1)
{
  path_file <- c('Depth/')
  file_entire <- paste(path_file, file, sep = '')
  illumina_depth <- read.csv(file = file_entire, sep = '\t', header = FALSE)
  
  colnames(illumina_depth) <- c('Contig_name','Base','Depth','Isolate')

  illumina_depth$Isol_Contig <- paste(illumina_depth$Isolate, illumina_depth$Contig_name, sep = '_')
  
  depth_contig_illumina <- illumina_depth %>%
    group_by(Isol_Contig) %>%
    summarise(Depth = mean(Depth))
  
  depth_contig_illumina$Isol_Contig <- gsub(pattern = 'phred20', replacement = '', x = depth_contig_illumina$Isol_Contig)
  
  depth_information <- merge(depth_contig_illumina, longest_segment_component, by = 'Isol_Contig')
  
  depth_information <- depth_information[order(depth_information$Contig_length, decreasing = TRUE),]
  
  depth_information$Norm_Depth <- depth_information$Depth/depth_information$Depth[1]
  all_illumina <- rbind(all_illumina, depth_information)
}



list_files <- read.csv(file = 'Depth/list_nanopore_files.txt', sep = '\t', header = FALSE)

all_ont <- NULL
for(file in list_files$V1)
{
  path_file <- c('Depth/')
  file_entire <- paste(path_file, file, sep = '')
  ont_depth <- read.csv(file = file_entire, sep = '\t', header = FALSE)
  
  colnames(ont_depth) <- c('Contig_name','Base','Depth','Isolate')

  ont_depth$Isol_Contig <- paste(ont_depth$Isolate, ont_depth$Contig_name, sep = '_')
  
  depth_contig_ont <- ont_depth %>%
    group_by(Isol_Contig) %>%
    summarise(Depth = mean(Depth))
  
  depth_contig_ont$Isol_Contig <- gsub(pattern = 'phred20', replacement = '', x = depth_contig_ont$Isol_Contig)
  
  depth_information <- merge(depth_contig_ont, longest_segment_component, by = 'Isol_Contig')
  
  depth_information <- depth_information[order(depth_information$Contig_length, decreasing = TRUE),]
  
  depth_information$Norm_Depth <- depth_information$Depth/depth_information$Depth[1]
  all_ont <- rbind(all_ont, depth_information)
}

illumina_plasmids <- subset(all_illumina, all_illumina$Prob_Chromosome <= 0.5)
ont_plasmids <- subset(all_ont, all_ont$Prob_Chromosome <= 0.5)


all_ont$Technology <- 'ONT'
all_illumina$Technology <- 'Illumina'

depth_final <- rbind(all_illumina, all_ont)
plasmids_final <- subset(depth_final, depth_final$Prediction == 'Plasmid')

plasmids_final %>%
  group_by(Technology) %>%
  dplyr::count()

setdiff(all_illumina$Isol_Contig, all_ont$Isol_Contig)

# The palette with black:
cbbPalette <- c("#000000", "#D55E00")


ggplot(plasmids_final, aes(x = Contig_length/1e3, y = log(Norm_Depth, base = 2), color = Technology)) + geom_line(aes(group = Isol_Contig), color = 'black', alpha = 0.2) + geom_point(size = 2.0) + labs(x = 'Longest contig in the component (kbp) - Only plasmid components', y = 'Depth relative to the chromosome (log2 scale)') + theme_bw() + scale_color_manual(values = cbbPalette) + labs(color = 'Technology') + guides(color = guide_legend(override.aes = list(size = 5))) # Figure 4 in results section 'Underrepresentation of small and medium plasmids in the ONT library' 


small_plasmids <- subset(plasmids_final, plasmids_final$Contig_length < 10e3)
medium_plasmids <- subset(plasmids_final, plasmids_final$Contig_length >= 10e3 & plasmids_final$Contig_length <= 50e3)
large_plasmids <- subset(plasmids_final, plasmids_final$Contig_length > 50e3)

small_plasmids %>%
  group_by(Technology) %>%
  summarise(log(mean(Norm_Depth),base =2)) # 1st result section 'Underrepresentation of small and medium plasmids in the ONT library'

 medium_plasmids %>%
  group_by(Technology) %>%
  summarise(log(mean(Norm_Depth),base =2)) # 2nd result section 'Underrepresentation of small and medium plasmids in the ONT library'

large_plasmids %>%
  group_by(Technology) %>%
  summarise(log(mean(Norm_Depth),base =2)) # 3rd result section 'Underrepresentation of small and medium plasmids in the ONT library' 


# Is there any plasmid not covered by ONT reads? 

missing_plasmids <- setdiff(illumina_plasmids$Isol_Contig, ont_plasmids$Isol_Contig)

missing_plasmids_info <- subset(plasmids_final, plasmids_final$Isol_Contig %in% missing_plasmids)

nrow(missing_plasmids_info) # 4th result section 'Underrepresentation of small and medium plasmids in the ONT library' 

nrow(subset(missing_plasmids_info, missing_plasmids_info$Contig_length < 1e4))
nrow(subset(missing_plasmids_info, missing_plasmids_info$Contig_length > 5e4))
```

# Results section: ONT only assemblies

``` r
# Evaluating the flye assemblies in the same manner that Unicycler reports 

list_assemblies <- read_table(file = 'Flye/new_list_flye.txt', col_names = FALSE)

flye_evaluation <- NULL
for(assembly in list_assemblies$X1)
{
  file_path <- paste('Flye/', assembly, sep = '')
  flye_assembly <- read.table(file = file_path, comment.char = "")
  filtered_graph <- data.frame(From_to = flye_assembly$V2, To_from = flye_assembly$V4) 
  
  # We use the function from igraph to create a network from a dataframe
  graph_pairs <<- graph_from_data_frame(filtered_graph, directed = FALSE)
  
  # We check that igraph considers the weight (reversed Mash distances) of the connections
  is.weighted(graph_pairs)
  
  components(graph_pairs)$no
  mean(components(graph_pairs)$csize)
  
  contigs_clusterg <- data.frame(edge = names(components(graph_pairs)$membership),
                                 Component = as.numeric(as.character(components(graph_pairs)$membership)))
  
  circularity_vector <- NULL
  
  for(component in contigs_clusterg$Component)
  {
    subgraph <- decompose(graph_pairs)[[component]]
    circularity <- is_connected(subgraph)
    if(length(V(subgraph)) > length(E(subgraph)))
       {
         circularity <- FALSE
       }
    circularity_vector <- append(x = circularity_vector, values = circularity, after = length(circularity_vector))
  }

  contigs_clusterg$Circularity <- circularity_vector
  
  
  contigs_per_component <- as.numeric(as.character(components(graph_pairs)$csize))
  
  contigs_clusterg$Number_contigs <- contigs_per_component[contigs_clusterg$Component]
  
  contigs_clusterg$Contig_name <- gsub(pattern = 'edge_', replacement = 'contig_', x = contigs_clusterg$edge)
  
  contigs_clusterg$edge <- NULL
  
  assembly_name <- gsub(pattern = 'phred20', replacement = '', x = flye_assembly$V7[1])
  
  path_assembly <- paste('Flye/',assembly_name, sep = '')
  
  path_assembly <- paste(path_assembly, 'phred20_assembly.fasta', sep = '')
  
  
  example_prediction <- suppressMessages(plasmid_classification(path_input_file = path_assembly, species = 'Escherichia coli', full_output = TRUE, min_length = 1))
  
  component_contigs <- merge(example_prediction, contigs_clusterg, by = 'Contig_name')
  
  independent_contigs <- subset(example_prediction,! example_prediction$Contig_name %in% contigs_clusterg$Contig_name)
  if(nrow(independent_contigs) > 0)
  {
      independent_contigs$Component <- independent_contigs$Contig_name
      independent_contigs$Number_contigs <- 1
      independent_contigs$Circularity <- 'FALSE'
      independent_contigs <- subset(independent_contigs, select = colnames(component_contigs))
      component_contigs <- rbind(component_contigs, independent_contigs)
  
  }
  
  length(unique(component_contigs$Component))
  
  n50_evaluation <- NULL
  
  for(component in unique(component_contigs$Component))
  {
    ind_component <- subset(component_contigs, component_contigs$Component == component)
    ind_component <- ind_component[order(ind_component$Contig_length, decreasing = TRUE),]
    
    component_size <- sum(ind_component$Contig_length)
    half_component_size <- component_size*0.5
    
    n50_sum <- 0
    
    prediction_contig <- ind_component$Prediction[1]
    prob_contig <- ind_component$Prob_Chromosome[1]
    circularity_contig <- ind_component$Circularity[1]
    
    for(contig in ind_component$Contig_name)
    {
      ind_contig <- subset(ind_component, ind_component$Contig_name == contig)
      n50_sum <- n50_sum + ind_contig$Contig_length
      if(n50_sum > half_component_size)
      {
        n50_df <- data.frame(Assembly = assembly_name, 
                   Component = component,
                   Prediction = prediction_contig,
                   Prob_chr = prob_contig,
                   Number_contigs = ind_component$Number_contigs,
                   Component_size = component_size,
                   N50 = ind_contig$Contig_length,
                   Circularity = circularity_contig)
        
        n50_evaluation <- rbind(n50_evaluation, n50_df)
    
        break
        
      }
      
    }
  }
  flye_evaluation <- rbind(flye_evaluation, n50_evaluation)
}

flye_evaluation <- flye_evaluation[!duplicated(flye_evaluation),]

flye_evaluation$Old_Component_Name <- flye_evaluation$Component
# The number of the components is not sorted based on their component size, let's tackle this 

flye_modification <- flye_evaluation
flye_modification$Component <- NULL
flye_modification$Isolate_Component <- NULL

flye_evaluation <- NULL

for(isol in unique(flye_modification$Assembly))
{
  particular_isolate <- subset(flye_modification, flye_modification$Assembly == isol)
  particular_isolate <- particular_isolate[order(particular_isolate$Component_size, decreasing = TRUE),]
  particular_isolate$Component<- c(1:nrow(particular_isolate))
  flye_evaluation <- rbind(particular_isolate, flye_evaluation)
  
}


flye_evaluation <- flye_evaluation[order(flye_evaluation$Component_size, decreasing = TRUE),]


flye_evaluation$Contiguity <- flye_evaluation$N50/flye_evaluation$Component_size


####### Creating Suppl. Table S3 

suppl_table_S3 <- flye_evaluation

suppl_table_S3$Contiguity <- suppl_table_S3$N50/suppl_table_S3$Component_size

suppl_table_S3 <- subset(suppl_table_S3, select = c('Assembly','Component','Component_size','N50','Contiguity','Number_contigs','Circularity','Prediction','Prob_chr'))

colnames(suppl_table_S3)[1] <- 'Isolate'
colnames(suppl_table_S3)[8] <- 'mlplasmids_prediction'
colnames(suppl_table_S3)[9] <- 'mlplasmids_chromosome_posterior_probability'

suppl_table_S3$Isolate <- gsub(pattern = 'flye_', replacement = '', x = suppl_table_S3$Isolate)

suppl_table_S3 <- merge(suppl_table_S3, barcode_illumina, by = 'Isolate')

suppl_table_S3 <- suppl_table_S3[order(suppl_table_S3$Barcode, decreasing = FALSE),]

suppl_table_S3$Barcode <- paste('Barcode', suppl_table_S3$Barcode, sep = '')

suppl_table_S3$Assembler <- 'Flye'

write.csv(x = suppl_table_S3, file = 'Suppl_Table_S3.csv', quote = FALSE, row.names = FALSE)

####### Suppl. Table S3 

# From here we can evaluate distinct questions 

# 0. Statistics behind bandage. General overview of the assembly 

bandage_flye <- read.csv(file = 'Flye/bandage_flye.txt', header = FALSE, sep = '\t')
bandage_flye$value <- str_split_fixed(string = bandage_flye$V1, pattern = ':', n = 2)[,2]
bandage_flye$Type <- str_split_fixed(string = bandage_flye$V1, pattern = ':', n = 2)[,1]

bandage_flye$value <- gsub(pattern = ' ', replacement = '', x = bandage_flye$value)
bandage_flye$value <- gsub(pattern = '%', replacement = '', x = bandage_flye$value)
bandage_flye$value <- as.numeric(as.character(bandage_flye$value))


##### Dead-ends
dead_ends_assembly_flye <- subset(bandage_flye, bandage_flye$Type == 'Dead ends')

round(mean(dead_ends_assembly_flye$value),2)
```

    ## [1] 5.15

``` r
round(median(dead_ends_assembly_flye$value),2)
```

    ## [1] 0

``` r
##### N50 (bp)

n50_assembly_flye <- subset(bandage_flye, bandage_flye$Type == 'N50 (bp)')

round(mean(n50_assembly_flye$value),2)
```

    ## [1] 4350608

``` r
round(median(n50_assembly_flye$value),2)
```

    ## [1] 4975011

``` r
##### Shortest node in the assembly 

shortest_node_assembly_flye <- subset(bandage_flye, bandage_flye$Type == 'Shortest node (bp)')

round(mean(shortest_node_assembly_flye$value),2)
```

    ## [1] 116177.7

``` r
round(median(shortest_node_assembly_flye$value),2)
```

    ## [1] 48056.5

``` r
# 1. What are the statistics of the largest component present in the assembly graph? 

flye_evaluation$Unique_name <- paste(flye_evaluation$Assembly, flye_evaluation$Component_size, sep = '_')

flye_evaluation <- flye_evaluation[order(flye_evaluation$Component_size, decreasing = TRUE),]

flye_evaluation$Contiguity <- flye_evaluation$N50/flye_evaluation$Component_size

largest_component_flye <- flye_evaluation[! duplicated(flye_evaluation$Assembly),] # We do have 94 samples, thus we can evaluate in a fair way both assemblers 

round(mean(largest_component_flye$Component_size)/1e6,2) # 1st result section 'ONT only assemblies' 
```

    ## [1] 4.45

``` r
round(median(largest_component_flye$Component_size)/1e6,2) # 2nd result section 'ONT only assemblies'
```

    ## [1] 4.98

``` r
round(mean(largest_component_flye$N50)/1e6,2) 
```

    ## [1] 4.42

``` r
round(median(largest_component_flye$N50)/1e6,2) 
```

    ## [1] 4.98

``` r
round(mean(largest_component_flye$Number_contigs),2) # 3rd result section 'ONT only assemblies'
```

    ## [1] 1.16

``` r
round(median(largest_component_flye$Number_contigs),2) # 4th result section 'ONT only assemblies'
```

    ## [1] 1

``` r
round(mean(largest_component_flye$Contiguity),2)
```

    ## [1] 0.98

``` r
round(median(largest_component_flye$Contiguity),2)
```

    ## [1] 1

``` r
circular_chr <- largest_component_flye[-grep(pattern = 'contig', x = largest_component_flye$Old_Component_Name),]
circular_chr <- subset(circular_chr, circular_chr$Number_contigs == 1) # 71 samples have a circular chromosome 

round(nrow(circular_chr)/94*100,0) # 5th result section 'ONT only assemblies'
```

    ## [1] 76

``` r
largest_component_flye$Unique_name <- paste(largest_component_flye$Assembly, largest_component_flye$Component_size, sep = '_')

# 2. What are the number of contigs produced per flye?

flye_contigs_assembly <- flye_evaluation %>%
  group_by(Assembly) %>%
  dplyr::count()

round(mean(flye_contigs_assembly$n),2)
```

    ## [1] 4.68

``` r
round(median(flye_contigs_assembly$n),2)
```

    ## [1] 3

``` r
# 3. Plasmid statistics 

all_pl_flye_information <- subset(flye_evaluation,! flye_evaluation$Unique_name %in% largest_component_flye$Unique_name) 

nrow(all_pl_flye_information) # 6th result section 'ONT only assemblies'
```

    ## [1] 346

``` r
all_pl_flye_information %>%
  group_by(Prediction) %>%
  dplyr::count()
```

    ## # A tibble: 2 x 2
    ## # Groups:   Prediction [2]
    ##   Prediction     n
    ##   <fct>      <int>
    ## 1 Chromosome   193
    ## 2 Plasmid      153

``` r
circular_pl <- subset(all_pl_flye_information, all_pl_flye_information$Prediction == 'Plasmid' & all_pl_flye_information$Number_contigs == 1)

remove_pl <- c(grep(pattern = 'contig', x = circular_pl$Old_Component_Name),grep(pattern = 'scaffold', x = circular_pl$Old_Component_Name))

circular_pl <- circular_pl[-remove_pl,] 

nrow(circular_pl) # 7th result section 'ONT only assemblies'
```

    ## [1] 120

``` r
round(nrow(circular_pl)/153*100,2) # 8th result section 'ONT only assemblies'
```

    ## [1] 78.43

``` r
# Number of small plasmids 

small_flye_pl <- subset(circular_pl, circular_pl$Component_size < 10e3)
nrow(small_flye_pl) # 8th result section 'ONT only assemblies'
```

    ## [1] 12

# Results section: Genome accuracy and completeness of hybrid and ONT-only assemblies

## Short-read assembly information of the 96 isolates

``` r
# Short-read assemblies 

assembly_current_strategy <- read_csv(file = 'Metadata/short-assembly_quality_current_strategy.txt', col_names = FALSE)
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   X1 = col_character()
    ## )

``` r
assembly_current_strategy$Strategy <- 'Barcode-96'

assembly_current_strategy$kmer <- str_split_fixed(string = assembly_current_strategy$X1, pattern = ' ', n = 4)[,3]
assembly_current_strategy$contigs <- str_split_fixed(string = assembly_current_strategy$X1, pattern = ' ', n = 20)[,10]
assembly_current_strategy$dead_ends <- str_split_fixed(string = assembly_current_strategy$X1, pattern = ' ', n = 30)[,21]

parsing_problem <- which(assembly_current_strategy$dead_ends == '')

assembly_current_strategy$dead_ends[parsing_problem] <- str_split_fixed(string = assembly_current_strategy$X1, pattern = ' ', n = 30)[parsing_problem,20]

assembly_current_strategy$contigs <- as.numeric(assembly_current_strategy$contigs)
assembly_current_strategy$dead_ends <- as.numeric(assembly_current_strategy$dead_ends)

assembly_gather <- assembly_current_strategy %>%
  gather(key = "Metric", value = "value", -X1, -Strategy, -kmer)

ggplot(data = assembly_gather, aes(x = Strategy, y = value)) + geom_boxplot() + geom_point() + facet_wrap(. ~ Metric, scales = 'free_y') + labs(title = 'Short-read assemblies comparison') + theme(text = element_text(size = 16))
```

![](evaluation_manuscript_files/figure-gfm/accuracy_completeness-1.png)<!-- -->

``` r
assembly_gather$X1 <- gsub(pattern = 'phred20_unicycler/unicycler.log', replacement = '', x = assembly_gather$X1)

assembly_gather$Illumina_id <- str_split_fixed(string = assembly_gather$X1, pattern = ':', n = 2)[,1]
assembly_gather$Illumina_id <- str_split_fixed(string = assembly_gather$Illumina_id, pattern = '_unicycler', n = 2)[,1]
assembly_gather$X1 <- NULL
```

## Quast statistics (Mismatches and Indels per 100 kbp)

``` bash
grep 'Total aligned length' quast_*/report.tsv > total_alignment.tsv
grep 'indels per 100 kbp' quast_*/report.tsv > indels_100_kbp.tsv 
grep 'mismatches per 100 kbp' quast_*/report.tsv > mm_100_kbp.tsv 
```

``` r
# The palette with black:
cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

mismatches <- read_tsv('Quast/spades_mm_100_kbp.tsv', col_names = FALSE)
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   X1 = col_character(),
    ##   X2 = col_double()
    ## )

``` r
mismatches$Isolate <- str_split_fixed(string = mismatches$X1, pattern = '/', n = 2)[,1]
mismatches$Isolate <- str_split_fixed(string = mismatches$Isolate, pattern = '_', n = 3)[,3]

colnames(mismatches)[2] <- 'Mismatches_100kbp'

mismatches$Assembler <- str_split_fixed(string = mismatches$X1, pattern = '_', n = 3)[,2]

indels <- read_tsv('Quast/spades_indels_100_kbp.tsv', col_names = FALSE)
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   X1 = col_character(),
    ##   X2 = col_double()
    ## )

``` r
indels$Isolate <- str_split_fixed(string = indels$X1, pattern = '/', n = 2)[,1]
indels$Isolate <- str_split_fixed(string = indels$Isolate, pattern = '_', n = 3)[,3]

colnames(indels)[2] <- 'Indels_100kbp'

indels$Assembler <- str_split_fixed(string = indels$X1, pattern = '_', n = 3)[,2]

mismatches %>%
  group_by(Assembler) %>%
  summarise(Mean=mean(Mismatches_100kbp), Max=max(Mismatches_100kbp), Min=min(Mismatches_100kbp), Median=median(Mismatches_100kbp), Std=sd(Mismatches_100kbp)) # Main results in the part of SNPs per 100 kbp
```

    ## # A tibble: 4 x 6
    ##   Assembler    Mean    Max   Min Median    Std
    ## * <chr>       <dbl>  <dbl> <dbl>  <dbl>  <dbl>
    ## 1 flye      130.    3108.   1.61  14.7  378.  
    ## 2 medaka    130.    3122.   1.33  12.6  381.  
    ## 3 spades      0.851   12.7  0      0.28   1.94
    ## 4 unicycler   6.93    61.2  0.22   5.24   7.85

``` r
indels %>%
  group_by(Assembler) %>%
  summarise(Mean=mean(Indels_100kbp), Max=max(Indels_100kbp), Min=min(Indels_100kbp), Median=median(Indels_100kbp), Std=sd(Indels_100kbp)) # Main results in the part of indels per 100 kbp 
```

    ## # A tibble: 4 x 6
    ##   Assembler     Mean     Max   Min Median      Std
    ## * <chr>        <dbl>   <dbl> <dbl>  <dbl>    <dbl>
    ## 1 flye      141.     1190.   26.3   41.5  237.    
    ## 2 medaka    126.     1223.    6.46  22.8  247.    
    ## 3 spades      0.0604    0.32  0      0.04   0.0653
    ## 4 unicycler   0.309     1.45  0      0.21   0.316

``` r
indels_cov <- merge(indels, barcode_information, by = 'Isolate')
mm_cov <- merge(mismatches, barcode_information, by = 'Isolate')

# Correlation between snps and coverage 

mm_cov_unicycler <- subset(mm_cov, mm_cov$Assembler == 'unicycler')
round(cor(x = mm_cov_unicycler$Mismatches_100kbp, y = mm_cov_unicycler$Bases, method = 'pearson'),2)
```

    ## [1] 0.01

``` r
mm_cov_flye <- subset(mm_cov, mm_cov$Assembler == 'flye')
round(cor(x = mm_cov_flye$Mismatches_100kbp, y = mm_cov_flye$Bases, method = 'pearson'),2) # Results, correlation of flye and ONT coverage 
```

    ## [1] -0.27

``` r
mm_cov_medaka <- subset(mm_cov, mm_cov$Assembler == 'medaka')
round(cor(x = mm_cov_medaka$Mismatches_100kbp, y = mm_cov_medaka$Bases, method = 'pearson'),2)
```

    ## [1] -0.27

``` r
# Correlation between indels and coverage 

indels_cov_unicycler <- subset(indels_cov, indels_cov$Assembler == 'unicycler')
round(cor(x = indels_cov_unicycler$Indels_100kbp, y = indels_cov_unicycler$Bases, method = 'pearson'),2)
```

    ## [1] -0.01

``` r
indels_cov_flye <- subset(indels_cov, indels_cov$Assembler == 'flye')
round(cor(x = indels_cov_flye$Indels_100kbp, y = indels_cov_flye$Bases, method = 'pearson'),2)
```

    ## [1] -0.61

``` r
indels_cov_medaka <- subset(indels_cov, indels_cov$Assembler == 'medaka')
round(cor(x = indels_cov_medaka$Indels_100kbp, y = indels_cov_medaka$Bases, method = 'pearson'),2)
```

    ## [1] -0.61

``` r
## Evaluation of the dead-ends and short-read contigs present in the SPAdes assembly graph 


indels_cov <- subset(indels_cov, indels_cov$Assembler %in% c('unicycler','spades'))

indels_cov$Illumina_id <- indels_cov$Isolate

assembly_spread <- assembly_gather %>%
  spread(key = Metric, value = value)

indels_cov <- merge(indels_cov, assembly_spread, by = 'Illumina_id')

gg_indels <- ggplot(indels_cov, aes(x = Barcode, y = Indels_100kbp, color = Assembler)) + geom_line(aes(group = Barcode), color = 'black', alpha = 1.0) + geom_point(size = 3.0, alpha = 0.6) + scale_x_continuous(breaks=c(1:96)) + scale_color_manual(values = cbbPalette) + coord_flip() + theme_bw() + labs(x = 'Barcode', y = 'Indels 100 kbp') + theme(axis.text.y = element_text(size=8), legend.position = 'top')

dead_ends_indels_uni<- subset(indels_cov, indels_cov$Assembler == 'unicycler')

plot_indels_cov <- ggplot(indels_cov, aes(x = Bases/1e6, y = Indels_100kbp)) + geom_point()  + facet_wrap(. ~ Assembler) + labs(y = 'Indels/100 kbp', x = 'ONT bases (Mbp)')


mm_cov <- subset(mm_cov, mm_cov$Assembler %in% c('unicycler','spades'))

mm_cov$Illumina_id <- mm_cov$Isolate

mm_cov <- merge(mm_cov, assembly_spread, by = 'Illumina_id')

gg_cov <- ggplot(mm_cov, aes(x = Barcode, y = Mismatches_100kbp, color = Assembler)) + geom_line(aes(group = Barcode), color = 'black', alpha = 1.0) + geom_point(size = 3.0, alpha = 0.6) + xlim(c(0,96)) + scale_x_continuous(breaks=c(1:96)) + scale_color_manual(values = cbbPalette) + coord_flip() + theme_bw() + labs(x = 'Barcode', y = 'SNPs 100 kbp') + theme(axis.text.y = element_text(size=8), legend.position = 'top')
```

    ## Scale for 'x' is already present. Adding another scale for 'x', which will
    ## replace the existing scale.

``` r
ggplot(mm_cov, aes(x = Isolate, y = Mismatches_100kbp, group = Assembler, fill = Assembler)) + geom_col(position = "dodge") + scale_fill_manual(values = cbbPalette) + coord_flip() + theme_bw()
```

![](evaluation_manuscript_files/figure-gfm/snps_indels-1.png)<!-- -->

``` r
plot_mm_cov <- ggplot(mm_cov, aes(x = Bases/1e6, y = Mismatches_100kbp)) + geom_point() + facet_wrap(. ~ Assembler) + labs(y = 'SNP/100 kbp', x = 'ONT bases (Mbp)') 

plot_grid(plot_indels_cov, plot_mm_cov, nrow = 2)
```

![](evaluation_manuscript_files/figure-gfm/snps_indels-2.png)<!-- -->

``` r
dead_ends_mm_uni<- subset(mm_cov, mm_cov$Assembler == 'unicycler')

gg_deadends_mm <- ggplot(dead_ends_mm_uni, aes(x = dead_ends, y = Mismatches_100kbp, size = contigs, color = contigs)) + geom_point() + theme_bw() + labs(x = 'Dead-ends in the short-read assembly graph', y = 'SNPs per 100 kbp') + scale_color_continuous(trans = 'reverse') + labs(size = 'SR Contigs', color = 'SR contigs')

gg_deadends_indels <- ggplot(dead_ends_indels_uni, aes(x = dead_ends, y = Indels_100kbp, size = contigs, color = contigs)) + geom_point() + theme_bw() + labs(x = 'Dead-ends in the short-read assembly graph', y = 'Indels per 100 kbp') + scale_color_continuous(trans = 'reverse') + labs(size = 'SR Contigs', color = 'SR contigs')

plot_grid(gg_deadends_mm, gg_deadends_indels, nrow = 2, labels = c("A","B")) # Supplemental Figure S2 
```

![](evaluation_manuscript_files/figure-gfm/snps_indels-3.png)<!-- -->

``` r
plot_grid(gg_cov, gg_indels, ncol = 2, labels = c('A','B')) # Supplemental Figure S1
```

![](evaluation_manuscript_files/figure-gfm/snps_indels-4.png)<!-- -->

## Busco searches (Completeness)

``` bash
grep 'C:' busco*/short*.txt > busco_summary.tsv 
```

``` r
busco_report <- read_tsv(file = 'Busco/busco_summary.tsv', col_names = FALSE)
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   X1 = col_character(),
    ##   X2 = col_character(),
    ##   X3 = col_logical()
    ## )

``` r
busco_report$Isolate <- str_split_fixed(string = busco_report$X1, pattern = '/', n = 2)[,1]
busco_report$Isolate <- str_split_fixed(string = busco_report$Isolate, pattern = '_', n = 3)[,3]

busco_report$Assembler <- str_split_fixed(string = busco_report$X1, pattern = '_', n = 3)[,2]

busco_report$Complete <- paste(str_split_fixed(string = busco_report$X2, pattern = ',', n = 3)[,1],str_split_fixed(string = busco_report$X2, pattern = ',', n = 3)[,2])

busco_report$Fragmented <- str_split_fixed(string = busco_report$X2, pattern = ',', n = 5)[,3]

busco_report$Missing <- str_split_fixed(string = busco_report$X2, pattern = ',', n = 5)[,4]

busco_report$N_Complete <- str_split_fixed(string = busco_report$Complete, pattern = '%', n = 2)[,1]
busco_report$N_Complete <- gsub(pattern = 'C:', replacement = '', x = busco_report$N_Complete)

busco_report$N_Fragmented <- str_split_fixed(string = busco_report$Fragmented, pattern = '%', n = 2)[,1]
busco_report$N_Fragmented <- gsub(pattern = 'F:', replacement = '', x = busco_report$N_Fragmented)

busco_report$N_Missing <- str_split_fixed(string = busco_report$Missing, pattern = '%', n = 2)[,1]
busco_report$N_Missing <- gsub(pattern = 'M:', replacement = '', x = busco_report$N_Missing)

busco_selection <- subset(busco_report, select=c('Isolate','Assembler','N_Complete','N_Fragmented','N_Missing'))

busco_gather <- busco_selection %>%
  gather(key = "Busco_Type", value = "Number_Genes", -Isolate,-Assembler)

busco_gather$Number_Genes <- as.numeric(busco_gather$Number_Genes)


busco_stats <- busco_gather %>%
  group_by(Assembler,Busco_Type) %>%
  summarise(Mean=mean(Number_Genes), Max=max(Number_Genes), Min=min(Number_Genes), Median=median(Number_Genes), Std=sd(Number_Genes), .groups = 'drop') # Main results of the section with the BUSCO description 

busco_stats$Mean <- round(busco_stats$Mean,2)

ggplot(busco_gather, aes(x = Busco_Type, y = Number_Genes)) + geom_boxplot() + geom_point() + facet_wrap(. ~ Assembler) + labs(y = 'Busco genes', x = 'Assessment')
```

![](evaluation_manuscript_files/figure-gfm/busco-1.png)<!-- -->

``` r
# Correlation between ONT depth and the number of completed busco genes 

busco_coverage <- merge(busco_gather, barcode_information, by = 'Isolate')

cov_complete_genes <- subset(busco_coverage, busco_coverage$Busco_Type == 'N_Complete')

plot_busco_cov <- ggplot(cov_complete_genes, aes(x = cov_complete_genes$Bases/1e6, y = cov_complete_genes$Number_Genes)) + geom_point() +  facet_wrap(. ~ Assembler) + labs(x = 'ONT bases (Mbp)', y = 'BUSCO completeness')

flye_busco_cov <- subset(cov_complete_genes, cov_complete_genes$Assembler == 'flye')
round(cor(x = flye_busco_cov$Number_Genes, y = flye_busco_cov$Bases, method = 'pearson'),2)
```

    ## [1] 0.78

``` r
medaka_busco_cov <- subset(cov_complete_genes, cov_complete_genes$Assembler == 'medaka')
round(cor(x = medaka_busco_cov$Number_Genes, y = medaka_busco_cov$Bases, method = 'pearson'),2)
```

    ## [1] 0.78

## Ideel test, ORF interrumptions

``` bash
for i in *.data; do isol=$(basename $i .data); awk -v var="$i" '{print $N,"\t", var}' $i; done > ideel_stats.txt
```

``` r
# Ideel analysis, let's look at the histograms 

ideel_test <- read.csv(file = 'Ideel/ideel_stats.txt', sep = '\t', header = FALSE)
ideel_test$ratio <- ideel_test$V1/ideel_test$V2

ideel_test$Strategy <- 'flye'

ideel_test$Strategy[grep(pattern = 'unicycler', x = ideel_test$V3)] <- 'unicycler'
ideel_test$Strategy[grep(pattern = 'medaka', x = ideel_test$V3)] <- 'medaka'

ideel_test$Isolate <- gsub(pattern = 'medaka_', replacement = '', x = ideel_test$V3)
ideel_test$Isolate <- gsub(pattern = 'flye_', replacement = '', x = ideel_test$Isolate)
ideel_test$Isolate <- gsub(pattern = 'phred20_unicycler.data', replacement = '', x = ideel_test$Isolate)
ideel_test$Isolate <- gsub(pattern = 'phred20.data', replacement = '', x = ideel_test$Isolate)
ideel_test$Isolate <- gsub(pattern = ' ', replacement = '', x = ideel_test$Isolate)

avg_interr_orfs <- ideel_test %>%
  filter(ideel_test$ratio < 0.9) %>%
  group_by(Strategy,V3,Isolate) %>%
  dplyr::count() 

avg_interr_orfs_coverage <- merge(avg_interr_orfs, barcode_information, by = 'Isolate')

plot_ideel_cov <- ggplot(avg_interr_orfs_coverage, aes(x = Bases/1e6, y = n)) + geom_point() +  facet_wrap(. ~ Strategy) + labs(x = 'ONT bases (Mbp)', y = 'Early terminated ORFs') 

avg_interr_orfs %>%
  group_by(Strategy) %>%
  summarise(mean = round(mean(n),2), median = round(median(n),2))
```

    ## # A tibble: 3 x 3
    ##   Strategy    mean median
    ## * <chr>      <dbl>  <dbl>
    ## 1 flye      1381.    772.
    ## 2 medaka    1128.    470 
    ## 3 unicycler   25.1    21

``` r
unicycler_interr_orfs <- subset(avg_interr_orfs, avg_interr_orfs$Strategy == 'unicycler')
round(mean(unicycler_interr_orfs$n),2)
```

    ## [1] 25.07

``` r
round(median(unicycler_interr_orfs$n),2)
```

    ## [1] 21

``` r
flye_interr_orfs <- subset(avg_interr_orfs, avg_interr_orfs$Strategy == 'flye')
round(mean(flye_interr_orfs$n),2)
```

    ## [1] 1380.64

``` r
round(median(flye_interr_orfs$n),2)
```

    ## [1] 772.5

``` r
medaka_interr_orfs <- subset(avg_interr_orfs, avg_interr_orfs$Strategy == 'medaka')
round(mean(medaka_interr_orfs$n),2)
```

    ## [1] 1127.99

``` r
round(median(medaka_interr_orfs$n),2)
```

    ## [1] 470

``` r
round(mean(avg_interr_orfs$n),0)
```

    ## [1] 839

``` r
round(median(avg_interr_orfs$n),0)
```

    ## [1] 456

``` r
summary(avg_interr_orfs$n)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##    11.0    27.0   455.5   838.8   906.5  4924.0

``` r
ggplot(ideel_test, aes(ratio)) + geom_histogram()
```

    ## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.

![](evaluation_manuscript_files/figure-gfm/ideel-1.png)<!-- -->

``` r
plot_grid(plot_mm_cov, plot_indels_cov, plot_ideel_cov, plot_busco_cov, nrow = 4, labels = c('A','B','C','D'))
```

    ## Warning: Use of `cov_complete_genes$Bases` is discouraged. Use `Bases` instead.

    ## Warning: Use of `cov_complete_genes$Number_Genes` is discouraged. Use
    ## `Number_Genes` instead.

![](evaluation_manuscript_files/figure-gfm/ideel-2.png)<!-- -->

``` r
flye_ideel_cov <- subset(avg_interr_orfs_coverage, avg_interr_orfs_coverage$Strategy == 'flye')
round(cor(x = flye_ideel_cov$n, y = flye_ideel_cov$Bases, method = 'pearson'),2)
```

    ## [1] -0.76

``` r
medaka_ideel_cov <- subset(avg_interr_orfs_coverage, avg_interr_orfs_coverage$Strategy == 'medaka')
round(cor(x = medaka_ideel_cov$n, y = medaka_ideel_cov$Bases, method = 'pearson'),2)
```

    ## [1] -0.78

# Results section: A long-read selection spanning the genome diversity inherent in a short-read collection

## Computing Jaccard distances from the Panaroo presence/absence matrix + tsne clustering

``` r
# Running this part of the code in the Frigessi server 

library(Rtsne)
library(tidyverse)
library(parallelDist)

presence_absence <- read.csv(file = 'gene_presence_absence.Rtab', sep = '\t')

rownames(presence_absence) <- presence_absence$Gene
presence_absence$Gene <- NULL
tr_presence_absence <- t(presence_absence)

# Jaccard distances

jaccard_dist <- parDist(as.matrix(tr_presence_absence), method = "fJaccard") # Computing Jaccard distances from the Panaroo presence/absence matrix

# tsne clustering, with different perplexity values and indicating the matrix is a distance matrix 

tsne5 <- Rtsne(jaccard_dist, is_distance = TRUE, dims = 2, perplexity = 5, verbose = TRUE, max_iter = 1000)
tsne30 <- Rtsne(jaccard_dist, is_distance = TRUE, dims = 2, perplexity = 30, verbose = TRUE, max_iter = 1000)
tsne50 <- Rtsne(jaccard_dist, is_distance = TRUE, dims = 2, perplexity = 50, verbose = TRUE, max_iter = 1000)
tsne100 <- Rtsne(jaccard_dist, is_distance = TRUE, dims = 2, perplexity = 100, verbose = TRUE, max_iter = 1000)

save.image(file='tsne.RData')
```

``` r
# Loading the image with all the variables from the previous chunk 

load(file = 'Panaroo/tsne.RData')

isolates <- rownames(tr_presence_absence)

isolates <- gsub(pattern = 'X', replacement = '', x = isolates)
isolates <- gsub(pattern = '.contigs_velvet', replacement = '', x = isolates)
isolates <- gsub(pattern = '\\.', replacement = '_', x = isolates)


# We load the file with the metadata that we had available for the project 

metadata_microreact <- read.csv(file = 'Microreact_input/microreact-project-iTjuukdyS-data.csv')

metadata_microreact <- metadata_microreact[match(isolates, metadata_microreact$Id),]

df_tsne5 <- data.frame(firstD = tsne5$Y[,1],
                       secondD = tsne5$Y[,2],
                       popPUNK = metadata_microreact$popPUNK_cluster__autocolour)

df_tsne30 <- data.frame(firstD = tsne30$Y[,1],
                       secondD = tsne30$Y[,2],
                       popPUNK = metadata_microreact$popPUNK_cluster__autocolour)

df_tsne50 <- data.frame(firstD = tsne50$Y[,1],
                       secondD = tsne50$Y[,2],
                       popPUNK = metadata_microreact$popPUNK_cluster__autocolour)

df_tsne100 <- data.frame(firstD = tsne100$Y[,1],
                       secondD = tsne100$Y[,2],
                       popPUNK = metadata_microreact$popPUNK_cluster__autocolour)

isolates <- colnames(presence_absence)


df_tsne5$popPUNK[which(df_tsne5$popPUNK > 5)] <- 'Other'
df_tsne30$popPUNK[which(df_tsne30$popPUNK > 5)] <- 'Other'
df_tsne50$popPUNK[which(df_tsne50$popPUNK > 5)] <- 'Other'
df_tsne100$popPUNK[which(df_tsne100$popPUNK > 5)] <- 'Other'

library(RColorBrewer)
library(cowplot)

ggtsne5 <- ggplot(data = df_tsne5, aes(x = firstD, y = secondD, color = as.factor(popPUNK))) + geom_point() + scale_color_brewer(palette="Dark2") + theme_bw() + labs(colors = 'Predominant popPUNK', title = 'tsne perplexity = 5, Jaccard distances (presence/absence Panaroo)',  color = 'popPUNK')

ggtsne30 <- ggplot(data = df_tsne30, aes(x = firstD, y = secondD, color = as.factor(popPUNK))) + geom_point() + scale_color_brewer(palette="Dark2") + theme_bw() + labs(colors = 'Predominant popPUNK', title = 'tsne perplexity = 30, Jaccard distances (presence/absence Panaroo)', color = 'popPUNK')

ggtsne50 <- ggplot(data = df_tsne50, aes(x = firstD, y = secondD, color = as.factor(popPUNK))) + geom_point() + scale_color_brewer(palette="Dark2") + theme_bw() + labs(colors = 'Predominant popPUNK', title = 'tsne perplexity = 50, Jaccard distances (presence/absence Panaroo)',  color = 'popPUNK')

ggtsne100 <- ggplot(data = df_tsne100, aes(x = firstD, y = secondD, color = as.factor(popPUNK))) + geom_point() + scale_color_brewer(palette="Dark2") + theme_bw() + labs(colors = 'Predominant popPUNK', title = 'tsne perplexity = 100, Jaccard distances (presence/absence Panaroo)',  color = 'popPUNK')

plot_grid(ggtsne5, ggtsne30, ggtsne50, ggtsne100)
```

![](evaluation_manuscript_files/figure-gfm/tsne-1.png)<!-- -->

``` r
# We decided to choose a perplexity value of 30, following the recommendation of other tools like Panini or the recent manuscript https://pubmed.ncbi.nlm.nih.gov/33837077/

ggtsne30 # This plot is coloured with the popPUNK lineages that we detected 
```

![](evaluation_manuscript_files/figure-gfm/tsne-2.png)<!-- -->

# Using kmeans to choose isolates ideal for long-read sequencing

``` r
# Running k-means on the tsne with a perplexity value of 30, this is the same hyperparameter value chosen in Panini as default 

dimensions <- data.frame(tsnefirst = df_tsne30$firstD, tsnesecond = df_tsne30$secondD)

seed_info <- 123

# We can try to capture the effect of an increase in the number of centroids (clusters) in the quality of the clustering 

kmeans_information <- NULL 

for(centroids in 1:250)
 {
   set.seed(seed_info)
   kmeans_clusters <- kmeans(centers = centroids, iter.max = 1e3, x = dimensions)
   kmeans_particular_centroids <- data.frame(Seed = seed_info, Centroids = centroids, totss = kmeans_clusters$totss, Tot_withinss = kmeans_clusters$tot.withinss, Betweenss = kmeans_clusters$betweenss)
   kmeans_information <- rbind(kmeans_information, kmeans_particular_centroids)
 
 }

kmeans_information$ratio <- kmeans_information$Betweenss/kmeans_information$totss

ggplot(kmeans_information, aes(x = Centroids, y = ratio)) + geom_point() + geom_line() + geom_vline(xintercept = 56) + labs(x = 'Number of clusters', y = 'totss/betweness')
```

![](evaluation_manuscript_files/figure-gfm/kmeans-1.png)<!-- -->

``` r
# Let's choose 96 as the number of centroids 

clusters_centroids <- 96
seed_info <- NULL

for(seed in 1:1e3)
{
   set.seed(seed)
   kmeans_clusters <- kmeans(centers = clusters_centroids, iter.max = 1e3, x = dimensions)
   kmeans_particular_seed <- data.frame(Seed = seed, Centroids = clusters_centroids, totss = kmeans_clusters$totss, Tot_withinss = kmeans_clusters$tot.withinss, Betweenss = kmeans_clusters$betweenss)
   seed_info <- rbind(seed_info, kmeans_particular_seed)
 
}

seed_info$ratio <- seed_info$Betweenss/seed_info$totss 

# Best seed, initialization of kmeans? 

seed_info <- seed_info[order(seed_info$ratio, decreasing = TRUE),]

set.seed(seed_info$Seed[1])

kmeans_96_clusters <- kmeans(centers = clusters_centroids, iter.max = 1e3, x = dimensions)

kmeans_96_clusters$betweenss/kmeans_96_clusters$totss 
```

    ## [1] 0.9973134

``` r
kmeans_96_clusters$tot.withinss
```

    ## [1] 8248.594

``` r
centroids <- ggplot(data = df_tsne30, aes(x = firstD, y = secondD, color = as.factor(popPUNK))) + geom_point(data = as.data.frame(kmeans_96_clusters$centers), aes(x = tsnefirst, y = tsnesecond), size = 5.0, color = 'black', shape = 18, alpha = 1.0) + geom_point(alpha = 0.5) + theme_bw() + theme(legend.position = 'right') + labs(color = 'popPUNK') + scale_color_brewer(palette="Dark2")

# Retrieving the coordinates of the centroids

centroid_df <- data.frame(tsnefirst = as.data.frame(kmeans_96_clusters$centers)$tsnefirst,
           tsnesecond = as.data.frame(kmeans_96_clusters$centers)$tsnesecond,
           Id = paste('centroid', 1:96, sep = '_'))

centroid_names <- unique(centroid_df$Id)

dimensions$Id <- isolates


# Now we can merge our original points together with the centroids defined by kmeans

tsne_centroids <- rbind(dimensions, centroid_df)

rownames(tsne_centroids) <- tsne_centroids$Id

tsne_centroids$Id <- NULL

# Calculating euclidean distances between all the points in the datafram tsne_centroids

euclidean_centroids_tsne <- dist(as.matrix(tsne_centroids), method = 'euclidean')
matrix_euclidean_centroids_tsne <- as.matrix(euclidean_centroids_tsne)

euclidean_centroids_tsne_gather <- as.data.frame(matrix_euclidean_centroids_tsne) %>%
  gather(key = "Id", value = "Euclidean_distance")

euclidean_centroids_tsne_gather$Pair_Id <- rownames(tsne_centroids)

final_selection <- NULL

for(centroid in unique(centroid_names))
{
  centroid_df <- subset(euclidean_centroids_tsne_gather, euclidean_centroids_tsne_gather$Id == centroid)
  centroids_isolate_df <- subset(centroid_df, centroid_df$Pair_Id %in% isolates)
  isolate_selected <- subset(centroids_isolate_df, centroids_isolate_df$Euclidean_distance == min(centroids_isolate_df$Euclidean_distance))
  final_selection <- rbind(final_selection, isolate_selected)
}

centroids <- ggplot(data = df_tsne30, aes(x = firstD, y = secondD)) + geom_point() + geom_point(data = as.data.frame(kmeans_96_clusters$centers), aes(x = tsnefirst, y = tsnesecond), size = 2.0, color = 'red', shape = 18, alpha = 1.0) + theme_bw() + theme(legend.position = 'right') + labs(title = 'k-means centroids')

coordinates_isolates_selected <- subset(dimensions, dimensions$Id %in% final_selection$Pair_Id)

# The palette with black:
cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#D55E00", "#CC79A7")

centroids_and_final_isolates <- ggplot(data = df_tsne30, aes(x = firstD, y = secondD, color = as.factor(popPUNK))) + geom_point(data = as.data.frame(kmeans_96_clusters$centers), aes(x = tsnefirst, y = tsnesecond), size = 3.0, color = 'black', shape = 18, alpha = 1.0) + geom_point(alpha = 0.5) + theme_bw() + theme(legend.position = 'right') + 
geom_text_repel(label = 'Selected', box.padding = 1.0, data = coordinates_isolates_selected, aes(x = tsnefirst, y = tsnesecond), size = 2.0, color = 'black', alpha = 1.0) + 
scale_color_manual(values = cbbPalette) + labs(color = 'popPUNK') + guides(color = guide_legend(override.aes = list(size = 5)))

centroids_and_final_isolates # Figure 1 from this section 
```

    ## Warning: ggrepel: 51 unlabeled data points (too many overlaps). Consider
    ## increasing max.overlaps

![](evaluation_manuscript_files/figure-gfm/kmeans-2.png)<!-- -->

``` r
sessionInfo()
```

    ## R version 3.6.3 (2020-02-29)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 20.04.2 LTS
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=nl_NL.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=nl_NL.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=nl_NL.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=nl_NL.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats4    parallel  stats     graphics  grDevices utils     datasets 
    ## [8] methods   base     
    ## 
    ## other attached packages:
    ##  [1] RColorBrewer_1.1-2  ggrepel_0.9.1       cowplot_1.1.1      
    ##  [4] forcats_0.5.0       stringr_1.4.0       dplyr_1.0.3        
    ##  [7] purrr_0.3.4         readr_1.4.0         tidyr_1.1.2        
    ## [10] tibble_3.0.5        ggplot2_3.3.3       tidyverse_1.3.0    
    ## [13] mlplasmids_1.0.0    seqinr_4.2-5        git2r_0.28.0       
    ## [16] kernlab_0.9-29      mlr_2.19.0          ParamHelpers_1.14  
    ## [19] Biostrings_2.54.0   XVector_0.26.0      IRanges_2.20.2     
    ## [22] S4Vectors_0.24.4    BiocGenerics_0.32.0 igraph_1.2.6       
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] nlme_3.1-144      fs_1.5.0          lubridate_1.7.9.2 progress_1.2.2   
    ##  [5] httr_1.4.2        tools_3.6.3       backports_1.2.1   utf8_1.1.4       
    ##  [9] R6_2.5.0          DBI_1.1.1         mgcv_1.8-31       colorspace_2.0-0 
    ## [13] ade4_1.7-16       withr_2.4.1       tidyselect_1.1.0  prettyunits_1.1.1
    ## [17] compiler_3.6.3    parallelMap_1.5.0 cli_2.2.0         rvest_0.3.6      
    ## [21] xml2_1.3.2        labeling_0.4.2    scales_1.1.1      checkmate_2.0.0  
    ## [25] digest_0.6.27     rmarkdown_2.6     pkgconfig_2.0.3   htmltools_0.5.1.1
    ## [29] dbplyr_2.0.0      rlang_0.4.10      readxl_1.3.1      rstudioapi_0.13  
    ## [33] BBmisc_1.11       generics_0.1.0    farver_2.0.3      jsonlite_1.7.2   
    ## [37] magrittr_2.0.1    Matrix_1.2-18     Rcpp_1.0.6        munsell_0.5.0    
    ## [41] fansi_0.4.2       lifecycle_0.2.0   stringi_1.5.3     yaml_2.2.1       
    ## [45] MASS_7.3-51.5     zlibbioc_1.32.0   grid_3.6.3        crayon_1.3.4     
    ## [49] lattice_0.20-40   haven_2.3.1       splines_3.6.3     hms_1.0.0        
    ## [53] knitr_1.30        pillar_1.4.7      fastmatch_1.1-0   reprex_1.0.0     
    ## [57] glue_1.4.2        evaluate_0.14     data.table_1.13.6 modelr_0.1.8     
    ## [61] vctrs_0.3.6       cellranger_1.1.0  gtable_0.3.0      assertthat_0.2.1 
    ## [65] xfun_0.20         broom_0.7.4       survival_3.1-8    ellipsis_0.3.1
