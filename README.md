Repository to reproduce the results present in the manuscript describing the ONT multiplexing approach and benchmark between hybrid and ONT-only assemblies 
================

# Code

In this [Markdown document](evaluation_manuscript.md), also available as [*.Rmd](evaluation_manuscript.Rmd), you will find all the code required to generate the results. 

If you have any issues/problems/questions, you can contact me by my mail address: s.a.alonso'ad'medisin.uio.no 

Below, one of my favourite figures from the paper!

![](evaluation_manuscript_files/figure-gfm/kmeans-2.png)<!-- -->
